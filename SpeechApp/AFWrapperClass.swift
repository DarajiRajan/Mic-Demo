//
//  AFWrapperClass.swift
//  Jhaveri
//
//  Created by Tanay Shah on 17/10/16.
//  Copyright © 2016 PardyPandaStudios. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD





public class NetworkManager
{
    public static var sharedManager: NetworkReachabilityManager =
    {
        let manager = NetworkReachabilityManager()
        // Add additional setup for the manager
        return manager!
    }()
}


class AFWrapperClass: NSObject
{
    
    //  static let BASE_URL = "http://52.64.157.49/Feels/api/" //base url For Production
    
    // static let BASE_URL = "http://52.64.157.49/Feels/api/"//base url For Staging//
    
    //  static let BASE_URL = "http://api.feelsfood.co/api/"//"http://13.54.172.253/Feels/api/"
    
    /*
     
     
     #if DEBUG
     
     static let BASE_URL = "http://admin.livetitles.live/livetitles/api/"
     
     
     #else
     
     //https://api.feelsfood.co/
     
     static let BASE_URL = "http://admin.livetitles.live/livetitles/api/"
     
     #endif
     */
    
    #if DEBUG
    
    ///
    
    
    #else
    
    //https://api.feelsfood.co/
    
    //static let BASE_URL = "http://admin.livetitles.live/livetitles/api/"
    
    #endif
    static let BASE_URL = "http://admin.livetitles.live/livetitles_staging/api/" // Staging
  //  static let BASE_URL = "http://admin.livetitles.live/livetitles_stagging/api/" // Production
    //    static let BASE_URL = "http://13.54.172.253/FeelsStaging/api/"
    
    //  static let BASE_URL = "http://13.54.172.253/Feels/api/"
    
    static var User_Token = ""
    static var User_Name = ""//User Name
    static var User_Profile = ""//User Profile Pic
    
    
    
    
    static let sharedInstance = AFWrapperClass()
    private let activityIndicator = UIActivityIndicatorView()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    
    let headers: HTTPHeaders =
        [
            "Authorization": "",
            "Content-Type":"application/json"
    ]
    
    class func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void)
    {
        URLSession.shared.dataTask(with: url)
        {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    
    //MARK: - Private Methods
    
    private func setupLoader()
    {
        removeLoader()
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = .gray
    }
    
    //MARK: - Public Methods
    
    
    func showLoader()
    {
        //        setupLoader()
        //
        //        let appDel = UIApplication.shared.delegate as! AppDelegate
        //        let holdingView = appDel.window!.rootViewController!.view!
        //
        //        DispatchQueue.main.async
        //            {
        //                self.activityIndicator.frame = CGRect(x: holdingView.frame.size.width - 60, y: holdingView.frame.size.height - 600, width: 50, height: 50)
        //
        //                self.activityIndicator.activityIndicatorViewStyle  = UIActivityIndicatorViewStyle.gray
        //                self.activityIndicator.startAnimating()
        //                holdingView.addSubview(self.activityIndicator)
        //                //UIApplication.shared.beginIgnoringInteractionEvents()
        //        }
    }
    
    func removeLoader()
    {
        DispatchQueue.main.async
            {
                self.activityIndicator.stopAnimating()
                self.activityIndicator.removeFromSuperview()
                //UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
    
    // MARK: POST Request API
    
    
    class func requestPOST3(_ strURL : String, params : [String : Any]?,method : HTTPMethod, headers : [String : String]?, success:@escaping (Any?) -> Void, failure:@escaping (Error) -> Void, selectLoader:Bool)
    {
        
        if(NetworkManager.sharedManager.isReachable == true)
        {
            
            Alamofire.request(strURL, method: method, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON
                { (responseObject) -> Void in
                    print(responseObject)
                    
                    if responseObject.result.isSuccess
                    {
                        let data = responseObject.result.value
                        success(data)
                        
                        //  UIApplication.shared.endIgnoringInteractionEvents()
                        
                        
                        
                        if(selectLoader == true)
                        {
                            SVProgressHUD.dismiss()
                        }
                        else
                        {
                            AFWrapperClass.sharedInstance.removeLoader()
                        }
                    }
                    if responseObject.result.isFailure
                    {
                        let error : Error = responseObject.result.error!
                        failure(error)
                        
                        UIApplication.shared.endIgnoringInteractionEvents()
                        
                        if(selectLoader == true)
                        {
                            SVProgressHUD.dismiss()
                        }
                        else
                        {
                            AFWrapperClass.sharedInstance.removeLoader()
                        }
                    }
            }
            
            if(selectLoader == true)
            {
                SVProgressHUD.show(withStatus: "Loading")
                SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.dark)
            }
            else
            {
                AFWrapperClass.sharedInstance.showLoader()
            }
        }
        else
        {
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "No Internet Connection."])
            failure(error)
            UIApplication.shared.endIgnoringInteractionEvents()
            NetworkManager.sharedManager.listener = { status in print("Network Status Changed: \(status)") }
            print("No Internet Connection.")
            
        }
    }
    
    
}


