//
//  ViewController.swift
//  SpeechApp
//
//  Created by Deivi Taka on 1/16/17.
//  Copyright © 2017 Deivi Taka. All rights reserved.
//

import UIKit
import Speech
import Firebase
import FirebaseDatabase

class ViewController: UIViewController, SFSpeechRecognizerDelegate ,UITextFieldDelegate{

    @IBOutlet weak var btnChangeLocation: UIButton!
    @IBOutlet weak var ViewTextViewContainer: UIView!
    @IBOutlet weak var lblCurrentLocation: UILabel!
    @IBOutlet weak var txtViewTranscription: UITextView!
    private var listening = false
    private var speechRecognizer: SFSpeechRecognizer?
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    var source = NSString()
    var enable = Bool()
    let deviceid = deviceUUID
    let osType = "ios"
    let osVersion = UIDevice.current.systemVersion
    let deviceCompany = "Apple"
    let modelName = UIDevice.current.modelName
    var locationId = String()
    let appType = "staging"
    var textFieldText = String()
    var newcount = 0
    var oldcount = 0
    //let appType = "production"

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnChangeLocation.layer.cornerRadius = 4.0
        self.ViewTextViewContainer.layer.borderWidth = 1
        self.ViewTextViewContainer.layer.borderColor = UIColor(red:173/255, green:181/255, blue:189/255, alpha: 1).cgColor
        
        // Initialize SFSpeechRecognizer
        speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
        speechRecognizer?.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        txtViewTranscription.isUserInteractionEnabled = false
        if let name = UserDefaults.standard.value(forKey: "SelectedLocation") as? String {
            if let city = UserDefaults.standard.value(forKey: "CITY") as? String {
                if let state = UserDefaults.standard.value(forKey: "STATE") as? String {
                    if let id = UserDefaults.standard.value(forKey: "LOCATIONID") as? String {
                    locationId = id
                        let boldText  = " \(name), \(city)"
                        let attrs = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 20)]
                        let attributedString = NSMutableAttributedString(string:boldText, attributes:attrs)
                        
                        let normalText = "Current Location :"
                        let normalString = NSMutableAttributedString(string:normalText)
                        normalString.append(attributedString)


                    lblCurrentLocation.attributedText = normalString
                    firebaseListnerStartAndStop()
                    }
                }
            }
            
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        promptForAnswer()
    }
    
    
    @IBAction func btnLocationChange(_ sender: UIButton) {
        promptForAnswer()
    }
    
    func promptForAnswer() {
        
        let alertController = UIAlertController(title: "Enter Passcode", message: "", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Save", style: .default) { (_) in
            if let field = alertController.textFields![0] as? UITextField {
                
                print(field.text)
                if (field.text == nil || (field.text?.isEmpty)! )
                {
                    let validationAlert = UIAlertController(title: "LiveTitles", message: "Passcode should not be left blank." , preferredStyle: UIAlertControllerStyle.alert)
                    
                    validationAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler:
                        { (action: UIAlertAction!) in
                            print("Ok")
                            
                    }))
                    
                    self.present(validationAlert, animated: true, completion: nil)
                }
                else
                {
                    self.callApiForLocationChange(oldLocation: " ", newLocation: " ", passcode: field.text!)
                }
            } else {
                print("please Passcode")
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Enter Passcode"
            textField.delegate = self
        }
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func firebaseListnerStartAndStop(){
        Database.database().reference().child("\(appType)").child("livestream").child("\(locationId)").child("source").removeAllObservers()
        Database.database().reference().child("\(appType)").child("livestream").child("\(locationId)").child("enabled").removeAllObservers()
        stopListening()
        
        //Check the source is 1 or 2
        Database.database().reference().child("\(appType)").child("livestream").child("\(locationId)").child("source").observe(.value, with: { (snapshot) in
            
            NSLog("sourceObserver : \(snapshot.value)")
            if let value = snapshot.value as? NSString{
                print(value)
                self.source = value
                self.startTheTranscription()
            }
        })
        
        // Check for System is Enabled or not
        Database.database().reference().child("\(appType)").child("livestream").child("\(locationId)").child("enabled").observe(.value, with: { (snapshot) in
            
            NSLog("enabledObserver : \(snapshot.value)")
            if let value = snapshot.value as? Bool {
                print(value)
                self.enable = value
                self.startTheTranscription()
            }
        })
    }
    
    func startTheTranscription(){
        if source == "1" && enable == true{
            startListening()
        }else{
            stopListening()
        }
    }
    
//    func permissionToStart(){
//        askMicPermission(completion: { (granted, message) in
//            DispatchQueue.main.async {
//                if self.listening {
//                    // Setup the text and stop the recording
//                    self.listening = false
//                    if granted {
//                        self.stopListening()
//                    }
//                } else {
//                    // Setup the text and start recording
//                    self.listening = true
//
//                   // self.txtViewTranscription.text = message
//
//                    if granted {
//                        self.startListening()
//                    }
//                }
//            }
//        })
//    }
    

    
    // MARK: - Private methods
    
    /**
        Check the status of Speech Recognizer authorization.
        - returns: A message, and if the access is granted.
     */
    private func askMicPermission(completion: @escaping (Bool, String) -> ()) {
        SFSpeechRecognizer.requestAuthorization { status in
            let message: String
            var granted = false
            
            switch status {
                case .authorized:
                    message = "Listening..."
                    granted = true
                    break
                    
                case .denied:
                    message = "Access to speech recognition is denied by the user."
                    break
                    
                case .restricted:
                    message = "Speech recognition is restricted."
                    break
                    
                case .notDetermined:
                    message = "Speech recognition has not been authorized yet."
                    break
            }
            
            completion(granted, message)
        }
    }

    /**
        Start listening to audio and try to convert it to text
     */
    private func startListening() {
        print("source \(source) enable \(enable)")
        if source == "1" && enable == true {
            print("Started")
            if recognitionTask != nil {
                recognitionTask?.cancel()
                recognitionTask = nil
            }
            
            // Start audio session
            let audioSession = AVAudioSession.sharedInstance()
            do {
                try audioSession.setCategory(AVAudioSessionCategoryRecord)
                try audioSession.setMode(AVAudioSessionModeMeasurement)
                try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
            } catch {
                txtViewTranscription.text = "An error occurred when starting audio session."
                return
            }
            
            // Request speech recognition
            recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
            
            guard let inputNode = audioEngine.inputNode else {
                fatalError("No input node detected")
            }
            
            guard let recognitionRequest = recognitionRequest else {
                fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
            }
            
            recognitionRequest.shouldReportPartialResults = true
            
            recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
                
                var isFinal = false
                
                if result != nil {
                    if self.textFieldText == "" {
                        self.txtViewTranscription.font = UIFont(name: "HelveticaNeue-Light", size:17)
                        self.txtViewTranscription.text = result?.bestTranscription.formattedString
                        self.scrollTheText()
                        self.wordSendToFirebase(string: self.txtViewTranscription.text)
                    }else{
                        self.txtViewTranscription.font = UIFont(name: "HelveticaNeue-Light", size:17)
                        self.txtViewTranscription.text = self.textFieldText + " " + (result?.bestTranscription.formattedString)!
                        self.scrollTheText()
                        self.wordSendToFirebase(string: self.txtViewTranscription.text)
                    }
                    
                    isFinal = result!.isFinal
                }
                
                if error != nil || isFinal {
                    self.textFieldText = self.txtViewTranscription.text
                    inputNode.removeTap(onBus: 0)
                    self.audioEngine.stop()
                    self.recognitionRequest = nil
                    self.recognitionTask = nil
                    self.startListening()
                }
            })
            
            let recordingFormat = inputNode.outputFormat(forBus: 0)
            inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
                self.recognitionRequest?.append(buffer)
            }
            
            audioEngine.prepare()
            
            do {
                try audioEngine.start()
            } catch {
                txtViewTranscription.text = "An error occurred starting audio engine"
            }
        }
        
    }
    func scrollTheText(){
        let range = NSMakeRange(txtViewTranscription.text.characters.count - 1, 0)
        txtViewTranscription.scrollRangeToVisible(range)
    }
    func wordSendToFirebase(string:String){
        print("the text is \(string)")
        let array = string.split(separator: " ")
       // print("array is \(array)")
        let components = string.components(separatedBy: .whitespacesAndNewlines)
        let words = components.filter { !$0.isEmpty }
        //print("The count is \(words.count)")
        newcount = words.count
        if newcount > oldcount {
            for data in oldcount..<newcount {
                print("push the data \(array[data])")
                Database.database().reference().child("\(self.appType)").child("livestream").child("\(self.locationId)").child("stream").childByAutoId().setValue(array[data])
                
            }
            oldcount = newcount
        }
        
    }
    
    /**
        Stop listening to audio and speech recognition
     */
    private func stopListening() {
        self.audioEngine.stop()
        self.recognitionRequest?.endAudio()
        self.recognitionRequest = nil
        self.recognitionTask = nil
        
    }
    
    func callApiForLocationChange(oldLocation : String , newLocation :String , passcode: String)
    {
        
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
        print(appVersion)
        
        let parameters: [String: String] =
            
            [
                "oldLocationId":oldLocation,
                "newLocationId":newLocation,
                "deviceUniqueId":deviceid,
                "os":osType,
                "osVersion":osVersion,
                "company":deviceCompany,
                "model":modelName,
                "type":"ChangeLocation",
                "passcode":passcode
        ]
        
        
        print("parameters: \(parameters)")
        
        AFWrapperClass.requestPOST3(AFWrapperClass.BASE_URL+"changeLocation" , params: parameters, method: .post, headers: nil , success: { (responce) in
            
            
            print ("ApiResponceCountLocationChange : \(String(describing: responce))")
            
            let getdata = responce as? NSDictionary
            
            getdata?.value(forKey: "Message")
            getdata?.value(forKey: "result")
            
            let strResult = getdata?.value(forKey: "result") as? String
            let strMessage = getdata?.value(forKey: "Message") as? String
            let location = getdata?.value(forKey: "location") as? NSDictionary
            
            
            
            print("new location data \(location)")
            if location != nil {
                let location2 = Location.init(dictionary: location!)
                UserDefaults.standard.set(location2?.name, forKey: "SelectedLocation")
                print("name of selected location \(location2?.name)")
                UserDefaults.standard.set(location2?.city, forKey: "CITY")
                UserDefaults.standard.set(location2?.state, forKey: "STATE")
                
                let boldText  = " \(UserDefaults.standard.value(forKey: "SelectedLocation") as! String), \(UserDefaults.standard.value(forKey: "CITY") as! String)"
                let attrs = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 20)]
                let attributedString = NSMutableAttributedString(string:boldText, attributes:attrs)
                
                let normalText = "Current Location :"
                let normalString = NSMutableAttributedString(string:normalText)
                normalString.append(attributedString)
                self.lblCurrentLocation.attributedText = normalString
                
                if let passcode = location2?.passcode {
                    print("passcode \(passcode)")
                    UserDefaults.standard.set(passcode, forKey: "PASSCODE")
                }
                Database.database().reference().child("\(self.appType)").child("livestream").child("\(self.locationId)").child("source").removeAllObservers()
                Database.database().reference().child("\(self.appType)").child("livestream").child("\(self.locationId)").child("enabled").removeAllObservers()

                if let locationid = location2?.id {
                    print("locationid \(locationid)")
                    self.locationId = locationid
                    UserDefaults.standard.set(locationid, forKey: "LOCATIONID")
                }
                self.firebaseListnerStartAndStop()
              
            }
            
            if strResult != "success"
                
            {
                
            }
            
        }, failure: { (error) in
            
            print ("ApiResponceFail With Error : \(error)")
            
            
        }, selectLoader: true)
        
        
    }
}


public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad6,11", "iPad6,12":                    return "iPad 5"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
        case "AppleTV5,3":                              return "Apple TV"
        case "AppleTV6,2":                              return "Apple TV 4K"
        case "AudioAccessory1,1":                       return "HomePod"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}

